package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Dhruvil Patel
 *
 */

public class LoginValidatorTest {

	
	// checking login name
	@Test
	public void testIsValidLoginNameRegular() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("dhruvil123");
		assertTrue( "Invalid user name", isValidLoginName);
	}
	@Test
	public void testIsValidLoginNameException() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("patel123");
		assertTrue( "Invalid user name", isValidLoginName);
		}
	@Test
	public void  testIsValidLoginNameBoundaryIn() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("tommy938");
		assertTrue( "Invalid user name", isValidLoginName);
		}
	
	@Test
	public void  testIsValidLoginNameBoundaryOut() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("sammysmith23");
		assertTrue( "Invalid user name", isValidLoginName);
		}

	//checking 6 alphanumeric characters
	@Test
	public void testIsValidLoginNameCharsRegular() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("username1");
		assertTrue( "Invalid user name", isValidLoginName);
	}
	@Test
	public void testIsValidLoginNameCharsException() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("username2");
		assertTrue( "Invalid user name", isValidLoginName);
		}
	@Test
	public void  testIsValidLoginNameCharsBoundaryIn() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("username56");
		assertTrue( "Invalid user name", isValidLoginName);
		}
	
	@Test
	public void  testIsValidLoginNameCharsBoundaryOut() {
		boolean isValidLoginName = LoginValidator.isValidLoginName("username45");
		assertTrue( "Invalid user name", isValidLoginName);
		}

	
	
	
}
