package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 
 * @author Dhruvil Patel
 *
 */

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		String myloginname = "^[a-z0-9_-]{3,15}$";
		Pattern pattern = Pattern.compile(myloginname, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(loginName);
		return matcher.find();
		
	}
}
